import copy
import time
import tkinter
from tkinter import Tk


def main():

    window = Tk()
    w = tkinter.Label(window, text="Waiting to start")
    w.pack()
    # Generating starting array
    starting_array = create_starting_array(8, 8)
    # Adding a few living cells for testing
    starting_array[1][1] = 1
    starting_array[1][2] = 1
    starting_array[2][1] = 1
    starting_array[2][2] = 1
    starting_array[3][3] = 1
    starting_array[3][4] = 1
    starting_array[4][3] = 1
    starting_array[4][4] = 1
    game_of_life(starting_array, window, w)


def create_starting_array(rows, columns):
    array = []
    for r in range(rows):
        array.append([])
    for r in range(rows):
        for c in range(columns):
            array[r].append([])
    for y in range(rows):
        for x in range(columns):
            array[y][x] = 0
    print_game(array)
    return array


def list_to_string(s):
    list_string = ''.join([str(elem) + "\n" for elem in s])
    return list_string


def game_of_life(a, window, w):
    # Deep copy of array, to allow editing
    array = copy.deepcopy(a)
    print()
    for y_in_y_axis in range(len(a)):
        for x_in_x_axis in range(len(a[y_in_y_axis])):
            array[y_in_y_axis][x_in_x_axis] = (check_neighbors(a, y_in_y_axis, x_in_x_axis))
    print_game(array)
    w.configure(text=list_to_string(array))
    window.after(0, w.update())
    time.sleep(0.1)
    # Recursive call to continue game
    game_of_life(array, window, w)


def check_neighbors(a, y_axis, x_axis):
    neighbouring = 0
    # Checks the <= 8 neighboring squares
    if y_axis + 1 in range(len(a)) and a[y_axis + 1][x_axis] == 1:
        neighbouring += 1

    if y_axis - 1 in range(len(a)) and a[y_axis - 1][x_axis] == 1:
        neighbouring += 1

    if x_axis + 1 in range(len(a[y_axis])) and a[y_axis][x_axis + 1] == 1:
        neighbouring += 1

    if x_axis - 1 in range(len(a[y_axis])) and a[y_axis][x_axis - 1] == 1:
        neighbouring += 1

    if x_axis - 1 in range(len(a[y_axis])) and y_axis - 1 in range(len(a)) and a[y_axis - 1][x_axis - 1] == 1:
        neighbouring += 1

    if x_axis + 1 in range(len(a[y_axis])) and y_axis - 1 in range(len(a)) and a[y_axis - 1][x_axis + 1] == 1:
        neighbouring += 1

    if x_axis - 1 in range(len(a[y_axis])) and y_axis + 1 in range(len(a)) and a[y_axis + 1][x_axis - 1] == 1:
        neighbouring += 1

    if x_axis + 1 in range(len(a[y_axis])) and y_axis + 1 in range(len(a)) and a[y_axis + 1][x_axis + 1] == 1:
        neighbouring += 1

    if neighbouring == 3:
        return 1

    if neighbouring == 2 and a[y_axis][x_axis] == 1:
        return 1

    return 0


def print_game(a):
    for x in range(len(a)):
        print(a[x])


if __name__ == '__main__':
    main()
